package es.kauron.jstudy.util;

import javafx.collections.ObservableList;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import java.util.ArrayList;
import java.util.List;

// Adapted from https://stackoverflow.com/a/52437193
public class DraggableRowFactory<S> {
    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");

    private final ObservableList<S> data;
    private final List<S> dragging = new ArrayList<>();

    public DraggableRowFactory(ObservableList<S> data) {
        this.data = data;
    }

    public TableRow<S> generator(TableView<S> table) {
        TableRow<S> row = new TableRow<>();
        row.setOnDragDetected((event) -> {
            if (!row.isEmpty() && table.getItems().size() == data.size()) {
                Integer index = row.getIndex();
                dragging.clear();
                dragging.addAll(table.getSelectionModel().getSelectedItems());
                Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent cc = new ClipboardContent();
                cc.put(SERIALIZED_MIME_TYPE, index);
                db.setContent(cc);
                event.consume();
            }
        });
        row.setOnDragOver((event) -> {
            Dragboard db = event.getDragboard();
            if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                if (row.getIndex() != (Integer) db.getContent(SERIALIZED_MIME_TYPE)) {
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    event.consume();
                }
            }
        });
        row.setOnDragDropped((event) -> {
            Dragboard db = event.getDragboard();
            if (!db.hasContent(SERIALIZED_MIME_TYPE))
                return;
            int dropIndex;
            S item = null;
            if (row.isEmpty()) {
                dropIndex = table.getItems().size();
            } else {
                dropIndex = row.getIndex();
                item = table.getItems().get(dropIndex);
            }
            int delta = 0;
            if (item != null) {
                while (dragging.contains(item)) {
                    delta = 1;
                    --dropIndex;
                    if (dropIndex < 0) {
                        item = null;
                        dropIndex = 0;
                        break;
                    }
                    item = table.getItems().get(dropIndex);
                }
            }
            data.removeAll(dragging);
            if (item != null)
                dropIndex = table.getItems().indexOf(item) + delta;
            else if (dropIndex != 0)
                dropIndex = table.getItems().size();

            table.getSelectionModel().clearSelection();

            for (S s : dragging) {
                data.add(dropIndex, s);
                table.getSelectionModel().select(dropIndex);
                dropIndex++;
            }

            event.setDropCompleted(true);
            dragging.clear();
            event.consume();
        });
        return row;
    }
}
