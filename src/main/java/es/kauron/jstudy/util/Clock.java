package es.kauron.jstudy.util;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Timer;
import java.util.TimerTask;

public class Clock {
    private static final short SEC_TO_MIN = 60;
    private short seconds = 0;
    private final Timer timer = new Timer();
    private TimerTask task;
    private final StringProperty time = new SimpleStringProperty();

    public Clock() {
        setObservable();
    }

    public StringProperty timeProperty() {
        return time;
    }

    public void play() {
        if (task != null) task.cancel();
        task = new TimerTask() {
            @Override
            public void run() {
                seconds++;
                Platform.runLater(() -> setObservable());
            }
        };
        timer.schedule(task, 0, 1000);
    }

    public void pause() {
        if (task != null)
            task.cancel();
        task = null;
    }

    public void stop() {
        timer.cancel();
        timer.purge();
    }

    private void setObservable() {
        time.set(String.format("%02d:%02d", seconds / SEC_TO_MIN, seconds % SEC_TO_MIN));
    }
}
