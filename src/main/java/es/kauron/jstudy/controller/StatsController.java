package es.kauron.jstudy.controller;

import es.kauron.jstudy.model.AnsweredItem;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StatsController implements Initializable {
    @FXML
    protected TableView<AnsweredItem> table;

    @FXML
    protected TableColumn<AnsweredItem, String> numCol, questionCol, answerCol, userAnswerCol, timeCol;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        numCol.setCellValueFactory(e -> e.getValue().indexProperty.asString());
        questionCol.setCellValueFactory(e -> e.getValue().item.questionProperty());
        answerCol.setCellValueFactory(e -> e.getValue().item.answerProperty());
        userAnswerCol.setCellValueFactory(e -> e.getValue().answerProperty);
        timeCol.setCellValueFactory(e -> Bindings.format("%.3f", e.getValue().timeProperty.divide(1e9)));
    }

    public void setData(List<AnsweredItem> answers) {
        table.setItems(FXCollections.observableArrayList(answers));
        // Set wrong answers in bold
        table.setRowFactory(param -> {
            TableRow<AnsweredItem> row = new TableRow<>();
            row.itemProperty().addListener((obj, o, n) ->
                    row.setStyle(n == null || n.isRight() ? "" : "-fx-font-weight: bold"));
            return row;
        });
    }
}
