package es.kauron.jstudy.controller;

import es.kauron.jstudy.model.AppPrefs;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    @FXML
    private CheckBox repeatMistakes, showFeedback, repeatImmediately, lockTabsOnTest;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        repeatImmediately.disableProperty().bind(repeatMistakes.selectedProperty().not());
        repeatImmediately.selectedProperty().bindBidirectional(AppPrefs.repeatImmediately);
        repeatMistakes.selectedProperty().bindBidirectional(AppPrefs.repeatWrong);
        showFeedback.selectedProperty().bindBidirectional(AppPrefs.showFeedback);
        lockTabsOnTest.selectedProperty().bindBidirectional(AppPrefs.lockTabsOnTest);
        repeatMistakes.selectedProperty().addListener((obj, o, n) -> {
            if (!n) repeatImmediately.setSelected(false);
        });
    }
}
