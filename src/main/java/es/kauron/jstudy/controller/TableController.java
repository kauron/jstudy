package es.kauron.jstudy.controller;

import es.kauron.jstudy.model.AppPrefs;
import es.kauron.jstudy.model.TestItem;
import es.kauron.jstudy.util.DraggableRowFactory;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TableController extends UndoController {
    @FXML
    private TableView<TestItem> table;
    @FXML
    private TextField newQuestionField, newAnswerField, searchField;
    @FXML
    private Button addButton, testButton;
    @FXML
    private TableColumn<TestItem, String> answerCol, questionCol;

    private FilteredList<TestItem> filtered;
    private ObservableList<TestItem> data;
    private Controller parent;
    private File file;
    StringProperty name;
    final BooleanProperty saved = new SimpleBooleanProperty();
    private final ObjectProperty<TestItem> editing = new SimpleObjectProperty<>(null);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        addButton.textProperty().bind(Bindings.when(addButton.disableProperty())
                .then("Question repeated")
                .otherwise(Bindings.when(editing.isNull())
                        .then("_Add item")
                        .otherwise("_Save item")));
        addButton.getTooltip().textProperty().bind(Bindings.when(editing.isNull())
                .then("Add an entry to the table")
                .otherwise("Save changes in the selected entry"));
        // Add context menu to Table
        MenuItem mEdit = new MenuItem("_Edit");
        mEdit.setOnAction(this::onEditAction);
        MenuItem mDel = new MenuItem("_Delete");
        mDel.setOnAction(this::onDeleteAction);
        MenuItem mDup = new MenuItem("D_uplicate");
        mDup.setOnAction(this::onDuplicateAction);
        mDup.disableProperty().bind(mDel.disableProperty());
        MenuItem mSwap = new MenuItem("_Swap");
        mSwap.setOnAction(this::onSwapAction);
        mSwap.disableProperty().bind(mDel.disableProperty());
        MenuItem mTest = new MenuItem("_Test selected");
        mTest.setOnAction(this::onTestSelectionAction);
        mTest.disableProperty().bind(mDel.disableProperty());
        MenuItem mMove = new MenuItem("_Move");
        mMove.setOnAction(this::onMoveData);
        mMove.disableProperty().bind(mDel.disableProperty());
        MenuItem mCopy = new MenuItem("_Copy");
        mCopy.setOnAction(this::onMoveData);
        mCopy.disableProperty().bind(mDel.disableProperty());
        mEdit.setDisable(true);
        mDel.setDisable(true);
        table.setContextMenu(new ContextMenu(mEdit, mDel, mDup, mSwap, new SeparatorMenuItem(),
                mTest, mCopy, mMove));

        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        answerCol.setCellValueFactory(e -> e.getValue().answerProperty());
        questionCol.setCellValueFactory(e -> e.getValue().questionProperty());
        table.getSelectionModel().getSelectedIndices().addListener((ListChangeListener<? super Integer>) obs -> {
            mEdit.setDisable(obs.getList().size() != 1);
            mDel.setDisable(obs.getList().size() == 0);
        });
        searchField.textProperty().addListener((obj, o, n) ->
                filtered.setPredicate((item) -> item.getQuestion().contains(n) || item.getAnswer().contains(n)));
        newQuestionField.textProperty().addListener((obj, o, n) -> {
            for (TestItem item : data) {
                if (item == editing.get()) continue;
                if (item.getQuestion().equals(n.trim())) {
                    addButton.setDisable(true);
                    return;
                }
            }
            addButton.setDisable(false);
        });
    }

    void setData(String name, List<TestItem> list, Controller controller, File file) {
        this.name = new SimpleStringProperty(name);
        this.data = FXCollections.observableArrayList(list);
        this.filtered = data.filtered((item) -> true);
        this.parent = controller;
        table.setRowFactory(new DraggableRowFactory<>(data)::generator);
        table.setItems(filtered);
        filtered.addListener((ListChangeListener<TestItem>) c ->
                testButton.setDisable(filtered.size() == 0));
        testButton.setDisable(filtered.size() == 0);
        this.file = file;
        saved.set(file != null);
    }

    void appendData(List<TestItem> list) {
        if (list.size() > 0) {
            saved.set(false);
            List<Action<TestItem>> actions = new ArrayList<>(list.size());
            for (TestItem i : list)
                actions.add(new AddAction<>(i, data));
            Collections.reverse(actions);
            doIt(new CollectionAction<>(actions));
        }
    }

    List<TestItem> getData() {return new ArrayList<>(data);}
    String getName() {return name.get();}

    void onMoveData(ActionEvent event) {
        List<TestItem> items = table.getSelectionModel().getSelectedItems();
        if (items.size() == 0) return;
        if (!parent.appendItemsToTab(items, this))
            return;
        if (((MenuItem) event.getTarget()).getText().contains("Move")) {
            onDeleteAction(event);
        }
    }

    protected void onSaveAction(ActionEvent event) {
        while (file == null) {
            FileChooser chooser = new FileChooser();
            chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JStudy file", "*.jsdb"));
            if (AppPrefs.lastDir != null) chooser.setInitialDirectory(AppPrefs.lastDir);
            chooser.setInitialFileName(name.get());
            file = chooser.showSaveDialog(table.getScene().getWindow());
            if (!file.getName().endsWith(".jsdb"))
                file = new File(file.getPath() + ".jsdb");
            if (file != null) {
                AppPrefs.lastDir = file.getParentFile();
                name.set(file.getName().substring(0, file.getName().lastIndexOf('.')));
            }
        }
        TestItem.saveTo(file, data);
        saved.set(true);
    }

    @FXML
    protected void onAddAction(ActionEvent event) {
        if (newQuestionField.getText().trim().isEmpty() && newAnswerField.getText().trim().isEmpty()) {
            newQuestionField.requestFocus();
            return;
        }
        Action<TestItem> action;
        if (editing.get() == null) {
            TestItem item = new TestItem(newQuestionField.getText().trim(), newAnswerField.getText().trim());
            action = new AddAction<>(item, data);
            saved.set(false);
        } else {
            action = new EditItemAction(editing.get(), newQuestionField.getText(), newAnswerField.getText());
        }
        doIt(action);
        onCancelAction(event);
    }

    protected void onEditAction(ActionEvent event) {
        ObservableList<TestItem> list = table.getSelectionModel().getSelectedItems();
        if (list.size() != 1) return;
        editing.set(list.get(0));
        newQuestionField.setText(list.get(0).getQuestion());
        newAnswerField.setText(list.get(0).getAnswer());
        saved.set(false);
        newQuestionField.requestFocus();
    }

    @FXML
    protected void onCancelAction(ActionEvent event) {
        if (editing.get() != null)
            editing.set(null);
        newQuestionField.setText("");
        newAnswerField.setText("");
        newQuestionField.requestFocus();
    }

    protected void onSwapAction(ActionEvent event) {
        if (table.getSelectionModel().getSelectedIndices().size() > 0) saved.set(false);
        Collection<Action<TestItem>> actionList = new ArrayList<>();
        for (TestItem item : table.getSelectionModel().getSelectedItems()) {
            actionList.add(Action.Reversible.genAction(item, () -> {
                String question = item.getQuestion();
                item.questionProperty().setValue(item.getAnswer());
                item.answerProperty().setValue(question);
            }));
        }
        doIt(new CollectionAction<>(actionList));
        table.requestFocus();
    }

    protected void onDuplicateAction(ActionEvent event) {
        if (table.getSelectionModel().getSelectedIndices().size() > 0) saved.set(false);
        Collection<Action<TestItem>> actionList = new ArrayList<>();
        for (int i : table.getSelectionModel().getSelectedIndices()) {
            TestItem item = new TestItem(filtered.get(i));
            Pattern p = Pattern.compile("^(.*) (\\d*)$");
            String base = item.getQuestion();
            int n = 2;
            Matcher m = p.matcher(base);
            if (m.find()) {
                n = Integer.parseInt(m.group(2)) + 1;
                base = m.group(1);
            }
            while (isAQuestion(base + " " + n))
                n++;
            item.questionProperty().setValue(base + " " + n);
            actionList.add(new AddAction<>(item, data));
        }
        doIt(new CollectionAction<>(actionList));
        table.requestFocus();
    }

    private boolean isAQuestion(String q) {
        for (TestItem t : data)
            if (t.getQuestion().equals(q))
                return true;
        return false;
    }

    protected void onDeleteAction(ActionEvent event) {
        if (table.getSelectionModel().getSelectedIndices().size() > 0) saved.set(false);
        Collection<Action<TestItem>> actionList = new ArrayList<>();
        for (TestItem i : table.getSelectionModel().getSelectedItems())
            actionList.add(Action.invert(new AddAction<>(i, data, data.indexOf(i))));
        doIt(new CollectionAction<>(actionList));
        table.requestFocus();
    }

    protected void onTestSelectionAction(ActionEvent event) {
        parent.newTest(table.getSelectionModel().getSelectedItems());
    }

    @FXML
    protected void onTestAction(ActionEvent event) {
        parent.newTest(filtered);
    }

    @FXML
    protected void onTableKeyEvent(KeyEvent event) {
        if (event.getCode().equals(KeyCode.DELETE)) {
            onDeleteAction(null);
        }
    }

    private final Timer timer = new Timer();
    private TimerTask timerTask = null;
    @FXML
    protected void onTableMouseClicked(MouseEvent event) {
        if (timerTask == null) {
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    timerTask = null;
                }
            };
            timer.schedule(timerTask, 200);
        } else {
            if (timerTask.cancel())
                onEditAction(null);
            timerTask = null;
        }
    }

    void stopTimer() {
        timer.cancel();
        timer.purge();
        if (timerTask != null)
            timerTask.cancel();
    }
}
