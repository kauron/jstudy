package es.kauron.jstudy.controller;

import es.kauron.jstudy.model.AnsweredItem;
import es.kauron.jstudy.model.AppPrefs;
import es.kauron.jstudy.model.TestItem;
import es.kauron.jstudy.util.Clock;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class TestController implements Initializable {
    private static final TestItem ending = new TestItem("That's it!", "");

    @FXML
    private Label question, prevQuestion, prevAnswer, correctAnswer, progressLabel, correctLabel;
    @FXML
    private TextField answer;
    @FXML
    private ProgressBar progress;
    @FXML
    private Pane feedback, buttonBox;
    @FXML
    private Button skipButton;
    @FXML
    private CheckBox pauseCheckBox;

    private final SimpleBooleanProperty correctingError = new SimpleBooleanProperty(false);
    private List<TestItem> list;
    private final IntegerProperty errors = new SimpleIntegerProperty(0);
    private final ObjectProperty<TestItem> item = new SimpleObjectProperty<>();
    private final IntegerProperty done = new SimpleIntegerProperty(0);
    private final List<AnsweredItem> answers = new ArrayList<>();
    private long timeQuestionStarted = 0;
    private Controller controller;
    // Time accounting
    private final Clock clock = new Clock();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        skipButton.disableProperty().bind(correctingError);
        feedback.visibleProperty().bind(AppPrefs.showFeedback);
        answer.disableProperty().bind(pauseCheckBox.selectedProperty());
        buttonBox.disableProperty().bind(pauseCheckBox.selectedProperty());
        pauseCheckBox.selectedProperty().addListener((obj, o, n) -> {
            if (n) clock.pause();
            else   clock.play();
        });
    }

    void setData(List<TestItem> list, Controller controller) {
        this.controller = controller;
        this.list = list;
        int total = list.size();
        progressLabel.textProperty().bind(Bindings.format(
                String.format("%%.2f%%%%     %%d / %d     %%d mistakes     %%s", total),
                done.multiply(100.0).divide(total), done,
                errors, clock.timeProperty()));
        progress.progressProperty().bind(done.divide((double) total));
        item.addListener((obj, o, n) -> {
            if (o != null) {
                prevQuestion.setText(o.getQuestion());
                correctAnswer.setText(o.getAnswer());
            }
            question.setText(n.getQuestion());
        });
        clock.play();
        onSkipAction(null);
    }

    @FXML
    private void onNextAction(ActionEvent event) {
        // Do not accept empty responses
        if (answer.getText().trim().isEmpty()) return;
        // Record the answer
        long timeElapsed = System.nanoTime() - timeQuestionStarted;
        AnsweredItem ai = new AnsweredItem(item.get(), answers.size() + 1, answer.getText().trim(), timeElapsed);
        answers.add(ai);
        prevAnswer.setText(answer.getText());

        correctAnswer.setVisible(!ai.isRight());
        correctLabel.setVisible(!ai.isRight());

        if (!ai.isRight()) {
            errors.set(errors.get() + 1);
            prevAnswer.setStyle("-fx-text-fill: #C40000;");
        } else {
            prevAnswer.setStyle("-fx-text-fill: #00C400;");
        }

        // Remove the question from the pool if the question was correctly answered or there is no repetition
        if (ai.isRight() || !AppPrefs.repeatWrong.get()) {
            if (!correctingError.get())
                done.set(done.get() + 1);
            correctingError.set(!ai.isRight() && AppPrefs.repeatImmediately.get());
            list.remove(item.get());
            if (list.size() == 0) {
                onEndAction(null);
                return;
            }
            chooseQuestion();
        } else if (AppPrefs.repeatImmediately.get()) {
            item.set(new TestItem(item.get()));
            if (!correctingError.get()) {
                correctingError.set(true);
                list.add(item.get());
            }
        } else {
            chooseQuestion();
        }
        answer.setText("");
        answer.requestFocus();
        timeQuestionStarted = System.nanoTime();
    }

    private void chooseQuestion() {
        answer.setText("");
        TestItem next;
        do
            next = list.get((int) (Math.random() * list.size()));
        while (list.size() > 1 && item.get() == next);
        item.set(next);
    }

    @FXML
    private void onSkipAction(ActionEvent event) {
        chooseQuestion();
        answer.setText("");
        answer.requestFocus();
        timeQuestionStarted = System.nanoTime();
    }

    @FXML
    private void onEndAction(ActionEvent event) {
        answer.setText("");
        item.set(ending);
        pauseCheckBox.setSelected(true);
        pauseCheckBox.setDisable(true);
        clock.stop();
        controller.createStatsTab(answers);
    }

    void stopTimer() {
        clock.stop();
    }
}
