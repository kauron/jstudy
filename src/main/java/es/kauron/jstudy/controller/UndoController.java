package es.kauron.jstudy.controller;

import es.kauron.jstudy.model.TestItem;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.Initializable;

import java.util.Collection;
import java.util.List;
import java.util.Stack;

public abstract class UndoController implements Initializable {
    private Stack<Action<?>> undoStack = new Stack<>();
    private Stack<Action<?>> redoStack = new Stack<>();
    public BooleanProperty undoProperty = new SimpleBooleanProperty(false);
    public BooleanProperty redoProperty = new SimpleBooleanProperty(false);

    public void doIt(Action<?> action) {
        action.action();
        redoStack.clear();
        redoProperty.set(false);
        if (undoStack.empty()) undoProperty.set(true);
        undoStack.push(action);
        undoProperty.set(true);
    }

    public void undo() {
        Action<?> a = undoStack.pop();
        a.reverse();
        if (undoStack.empty())
            undoProperty.set(false);
        if (redoStack.empty())
            redoProperty.set(true);
        redoStack.push(a);
    }

    public void redo() {
        Action<?> a = redoStack.pop();
        a.action();
        if (redoStack.empty())
            redoProperty.set(false);
        if (undoStack.empty())
            undoProperty.set(true);
        undoStack.push(a);
    }

    public interface Call {
        void call();
    }

    public abstract static class Action<A> {
        protected final A item;

        public Action(A item) {
            this.item = item;
        }

        public abstract void action();
        public abstract void reverse();

        public static <X> Action<X> invert(Action<X> action) {
            return new Action<X>(action.item) {
                @Override
                public void action() {
                    action.reverse();
                }

                @Override
                public void reverse() {
                    action.action();
                }
            };
        }

        public static <E> Action<E> genAction(E item, Call doit, Call undoit) {
            return new Action<E>(item) {
                @Override
                public void action() { doit.call(); }
                @Override
                public void reverse() { undoit.call(); }
            };
        }

        public abstract static class Reversible<R> extends Action<R> {
            public Reversible(R item) {
                super(item);
            }

            @Override
            public void reverse() {
                action();
            }

            public static <R> Action<R> genAction(R item, Call doit) {
                return new Reversible<R>(item) {
                    @Override
                    public void action() { doit.call(); }
                };
            }
        }
    }

    public static class AddAction<N> extends Action<N> {
        private final List<N> list;
        private final int index;

        public AddAction(N item, List<N> list) {
            this(item, list, list.size());
        }

        public AddAction(N item, List<N> list, int index) {
            super(item);
            this.list = list;
            this.index = index;
        }

        @Override
        public void action() {
            list.add(index, item);
        }

        @Override
        public void reverse() {
            list.remove(item);
        }
    }

    public static class CollectionAction<N> extends Action<Collection<Action<N>>> {
        public CollectionAction(Collection<Action<N>> item) {
            super(item);
        }

        @Override
        public void action() {
            for (Action<N> a : item)
                a.action();
        }

        @Override
        public void reverse() {
            for (Action<N> a : item)
                a.reverse();
        }
    }

    public static class EditItemAction extends Action.Reversible<TestItem> {
        private String question, answer;
        public EditItemAction(TestItem item, String newQuestion, String newAnswer) {
            super(item);
            this.question = newQuestion;
            this.answer   = newAnswer;
        }

        @Override
        public void action() {
            String q = item.getQuestion();
            String a = item.getAnswer();
            item.questionProperty().setValue(question);
            item.answerProperty().setValue(answer);
            question = q;
            answer = a;
        }
    }
}
