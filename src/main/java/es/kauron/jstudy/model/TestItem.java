package es.kauron.jstudy.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class TestItem {
    public static final String TAB = "\t", COLONS = "::", SEMICOLON = ";", COMMA = ",";

    private final StringProperty question;
    private final StringProperty answer;

    private TestItem(SimpleStringProperty question, SimpleStringProperty answer) {
        this.question = question;
        this.answer = answer;
    }

    public TestItem(TestItem item) {
        this(item.getQuestion(), item.getAnswer());
    }

    public TestItem(String question, String answer) {
        this(new SimpleStringProperty(question), new SimpleStringProperty(answer));
    }

    public String getQuestion() {
        return question.get();
    }

    public StringProperty questionProperty() {
        return question;
    }

    public String getAnswer() {
        return answer.get();
    }

    public StringProperty answerProperty() {
        return answer;
    }

    public boolean isValid() {
        return !question.get().isEmpty() && !answer.get().isEmpty();
    }

    public static void saveTo(File file, List<TestItem> data) {
        try (OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream(file), StandardCharsets.UTF_8.newEncoder())) {
            for (TestItem item : data) {
                writer.write(item.getQuestion() + "::" + item.getAnswer() + "\n");
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<TestItem> loadFrom(File file, String separator) {
        List<TestItem> list = new ArrayList<>();
        String encoding = "UTF-8";
        if (separator.equals(COMMA) || separator.equals(SEMICOLON) || separator.equals(TAB))
            encoding = "ISO-8859-1";
        try (Scanner in = new Scanner(new FileInputStream(file), encoding)) {
            while (in.hasNextLine()) {
                String line = in.nextLine();
                if (!line.matches("..*" + separator + "..*")) continue;
                int index = line.indexOf(separator);
                String question = line.substring(0, index).trim();
                String answer   = line.substring(index + separator.length()).trim();
                index = answer.indexOf(separator);
                if (index != -1)
                    answer = answer.substring(0, index);
                if (answer.isEmpty()) continue;
                list.add(new TestItem(question, answer));
            }
        } catch (FileNotFoundException | InputMismatchException e) {
            e.printStackTrace();
        }
        return list;
    }
}
