package es.kauron.jstudy.model;

import es.kauron.jstudy.Main;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.File;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class AppPrefs {
    private static final Preferences prefs = Preferences.userNodeForPackage(Main.class);
    public static final BooleanPref repeatWrong = new BooleanPref(false, "repeatWrong");
    public static final BooleanPref showFeedback = new BooleanPref(true, "showFeedback");
    public static final BooleanPref repeatImmediately = new BooleanPref(false, "repeatImmediately");
    public static final BooleanPref lockTabsOnTest = new BooleanPref(false, "lockTabsOnTest");
    public static File lastDir;

    static {
        // Load data (booleanprefs are loaded automatically)
        String dirPath = prefs.get("lastDir", "");
        if (!dirPath.isEmpty()) lastDir = new File(dirPath);
    }

    private static void flushPrefs() {
        try {
            prefs.flush();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }
    }

    public static void save() {
        prefs.put("lastDir", lastDir == null ? "" : lastDir.getAbsolutePath());
        flushPrefs();
    }

    public static String getVersion() {
        String v = new Scanner(AppPrefs.class.getResourceAsStream("/version.txt")).nextLine();
        assert v.matches("\\d\\.\\d\\.\\d");
        return v;
    }

    public static class BooleanPref extends SimpleBooleanProperty {
        private final String name;

        BooleanPref(boolean defValue, String name) {
            super(defValue);
            this.name = name;
            set(prefs.getBoolean(name, defValue));
            addListener((obj, o, n) -> this.save());
        }

        void save() {
            prefs.putBoolean(name, get());
            flushPrefs();
        }
    }
}
