package es.kauron.jstudy.model;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;

public class AnsweredItem {
    public final ReadOnlyStringWrapper answerProperty;
    public final ReadOnlyLongWrapper timeProperty;
    public final ReadOnlyIntegerProperty indexProperty;
    public final TestItem item;

    public AnsweredItem(TestItem item, int index, String answer, long timeNano) {
        this.item = item;
        this.indexProperty = new ReadOnlyIntegerWrapper(index);
        this.answerProperty = new ReadOnlyStringWrapper(answer);
        this.timeProperty = new ReadOnlyLongWrapper(timeNano);
    }

    public boolean isRight() {
        return item.getAnswer().equals(answerProperty.get());
    }
}
