package es.kauron.jstudy;

import es.kauron.jstudy.model.AppPrefs;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Main.class.getResource("view/main.fxml"));
        primaryStage.setTitle("JStudy");
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("img/icon.png")));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setOnHiding(event -> AppPrefs.save());
    }


    public static void main(String[] args) {
        launch(args);
    }
}
